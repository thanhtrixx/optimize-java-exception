package jmh.advanced;

public class NSTException extends Exception {

  private static final StackTraceElement[] EMPTY_STACK_TRACE = new StackTraceElement[0];

  public NSTException(String message) {
    super(message);
  }


  @Override
  public StackTraceElement[] getStackTrace() {
    return EMPTY_STACK_TRACE;
  }

  @Override
  public Throwable fillInStackTrace() {
    return this;
  }

}
