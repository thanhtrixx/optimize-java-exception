package jmh.advanced;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@Warmup(iterations = 25, time = 10, timeUnit = TimeUnit.MICROSECONDS)
@Measurement(iterations = 25, time = 10, timeUnit = TimeUnit.MICROSECONDS)
@Fork(1)
@State(Scope.Thread)
public class Jmh {

//  @Param({"1", "2", "3"})
  @Param({"1"})
  private static String param;

  @Benchmark
  public void callToThrowException(Blackhole blackhole) {
    try {
      methodThrowException(param);
    } catch (Exception e) {
      blackhole.consume(e);
    }
  }

  @Benchmark
  public void callToThrowNSTException(Blackhole blackhole) {
    try {
      methodThrowNSTException(param);
    } catch (Exception e) {
      blackhole.consume(e);
    }
  }

  @Benchmark
  public void callNotThrowException(Blackhole blackhole) {
    try {
      doSomeThing(param);
    } catch (Exception e) {
      blackhole.consume(e);
    }
  }

  private String doSomeThing(String paramInput) {
    return "process " + paramInput;
  }

  private String methodThrowException(String paramInput) throws Exception {
    String result = "process " + paramInput;
    throw new Exception("Exception here");
  }

  private String methodThrowNSTException(String paramInput) throws Exception {
    String result = "process " + paramInput;
    throw new NSTException("NSTException here");
  }

}
