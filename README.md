# Optimize Java Exception

### Run benchmark:
`./gradlew --no-daemon clean jmh`

### View result:
`cat build/reports/jmh/results.txt`
